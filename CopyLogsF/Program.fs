﻿open System

let rec directoryCopy srcPath dstPath copySubDirs prefixForFiles =
    
    printfn "START %A ..." dstPath
    
    if not <| System.IO.Directory.Exists(srcPath) then
        let msg = System.String.Format("Source directory does not exist or could not be found: {0}", srcPath)
        raise (System.IO.DirectoryNotFoundException(msg))

    if not <| System.IO.Directory.Exists(dstPath) then
        System.IO.Directory.CreateDirectory(dstPath) |> ignore

    let srcDir = new System.IO.DirectoryInfo(srcPath)

    for file in srcDir.GetFiles() do
        let temppath = System.IO.Path.Combine(dstPath, prefixForFiles+"_"+file.Name)
        file.CopyTo(temppath, false)  |> ignore // !!!

    if copySubDirs then
        for subdir in srcDir.GetDirectories() do
            let dstSubDir = System.IO.Path.Combine(dstPath, subdir.Name)
            directoryCopy subdir.FullName dstSubDir copySubDirs prefixForFiles
    printfn "END   %A " dstPath

            
            
let dirCopyTest () =
            
    let dirFrom="C:\\test\\logs1"
    printfn  "fromF= %A" dirFrom
            
    let dirTo="C:\\test\\logs2"
    printfn  "toF= %A" dirTo
                
    let prefixForFiles="qwe12"
            
    directoryCopy dirFrom dirTo true prefixForFiles
    0 // return an integer exit code



[<EntryPoint>]
let main argv =
    
    //dirCopyTest
    
    if not <| (argv.Length=3) 
    then 
        printfn "Give me 3 args: 1- dirFrom; 2- dirTo; 3- prefixForFiles"
    else
        let dirFrom=argv.[0]
        printfn  "fromF= %A" dirFrom
    
        let dirTo=argv.[1]
        printfn  "toF= %A" dirTo
        
        let prefixForFiles=argv.[2]
        printfn  "prefixForFiles= %A" prefixForFiles

        printfn "Press ENTER to start !"
        System.Console.ReadLine() |> ignore
    
        printfn "START..."
        directoryCopy dirFrom dirTo true prefixForFiles
        printfn "END"

    printfn "Press ENTER to exit !"
    System.Console.ReadLine() |> ignore
    0